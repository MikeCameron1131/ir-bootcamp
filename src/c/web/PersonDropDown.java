package c.web;

import java.sql.Connection;
import java.util.List;
import c.data.Person;

public class PersonDropDown
{
private static final String Blank = "";
private static final int SortBy = 0;
public static StringBuilder personDropDown(Connection connection, int selected) throws Exception
{
    List<Person> list = Person.select(connection, Blank, SortBy);
    StringBuilder sb = new StringBuilder();
    sb.append("<option value='0' ");
    if (selected == 0)
    {
        sb.append("selected");
    }
    sb.append(">Select</option>");
    for (Person person : list)
    {
        sb.append("<option value='" + person.Id + "'");
        if (selected == person.Id)
        {
            sb.append("selected");
        }
        sb.append(">" + person.FirstName + "</option>");
    }
    sb.append("</select>");
    return sb;
}
}