package c.web;

import java.sql.Connection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import c.data.Department;
import c.data.Person;

public class DepartmentTableBean extends BaseBean
{
public static final String pageType = "Department";
public String keyword = getString("keyword");
public int sort = getInt("sort");
public StringBuilder sb = new StringBuilder();
public SearchByForm form = new SearchByForm();
public DepartmentTableBean(HttpServletRequest request, HttpServletResponse response)
{
    super(request, response);
    url = "department.jsp";
    beanWrap(false);
}
@Override
protected void prepare(Connection connection) throws Exception
{
    sb = new StringBuilder();
    if (getString("reset").equals("Reset"))
    {
        resetPage();
        return;
    }
    List<Department> list = Department.select(connection, keyword, sort);
    int x = 0;
    Person person = new Person();
    for (Department department : list)
    {
        person.Id = department.Manager;
        person.selectById(connection);
        String manager = person.Id == 0 ? "None" : person.FirstName;
        sb.append("<tr");
        if ((x % 2) == 0)
        {
            sb.append(" class='trGreen'");
        }
        sb.append(">").append("<td class='rightAlign'>" + department.Id + "</td>").append(
                "<td class='leftAlign'><a href='../ProjectC/departmentdtl.jsp?id=" + department.Id
                        + "&sort=" + getInt("sort") + "&keyword=" + getString("keyword") + "'>"
                        + department.Name + "</a></td>").append("<td class='leftAlign'>" + manager
                                + "</td>").append("</tr>");
        x++;
    }
}
@Override
protected void process(Connection connection) throws Exception
{
    // TODO Auto-generated method stub
}
}
