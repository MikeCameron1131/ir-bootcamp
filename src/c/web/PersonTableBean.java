package c.web;

import java.sql.Connection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import c.data.Person;

public class PersonTableBean extends BaseBean
{
public static final String pageType = "Person";
public static StringBuilder sb = new StringBuilder();
public String keyword = getString("keyword");
public int sort = getInt("sort");
public SearchByForm form = new SearchByForm();
public PersonTableBean(HttpServletRequest request, HttpServletResponse response)
{
    super(request, response);
    url = "person.jsp";
    beanWrap(false);
}
@Override
protected void prepare(Connection connection) throws Exception
{
    sb = new StringBuilder();
    if (getString("reset").equals("Reset"))
    {
        resetPage();
        return;
    }
    List<Person> list = Person.select(connection, getString("keyword"), getInt("sort"));
    int x = 0;
    for (Person person : list)
    {
        sb.append("<tr");
        if ((x % 2) == 0)
        {
            sb.append(" class='trGreen'");
        }
        sb.append(">").append("<td class='rightAlign'>" + person.Id + "</td>").append(
                "<td class='leftAlign'><a href='../ProjectC/persondtl.jsp?id=" + person.Id
                        + "&sort=" + getInt("sort") + "&keyword=" + getString("keyword") + "'>"
                        + person.FirstName + "</a></td>").append("<td class='rightAlign'>"
                                + person.PhoneNumber + "</td>").append("</tr>");
        x++;
    }
}
@Override
protected void process(Connection connection) throws Exception
{
    // TODO Auto-generated method stub
}
}
