package c.web;

import java.sql.Connection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import c.data.Country;

public class CountryTableBean extends BaseBean
{
public static StringBuilder sb = new StringBuilder();
public String keyword = getString("keyword");
public int sort = getInt("sort");
public final String pageType = "Country";
public SearchByForm form = new SearchByForm();
public CountryTableBean(HttpServletRequest request, HttpServletResponse response)
{
    super(request, response);
    sb = new StringBuilder();
    url = "country.jsp";
    beanWrap(false);
}
@Override
protected void prepare(Connection connection) throws Exception
{
    if (getString("reset").equals("Reset"))
    {
        resetPage();
        return;
    }
    List<Country> list = Country.select(connection, getString("keyword"), getInt("sort"));
    int x = 0;
    for (Country country : list)
    {
        sb.append("<tr");
        if ((x % 2) == 0)
        {
            sb.append(" class='trGreen'");
        }
        sb.append(">").append("<td class='rightAlign'>" + country.Id + "</td>").append(
                "<td class='leftAlign'><a href='../ProjectC/countrydtl.jsp?id=" + country.Id
                        + "&sort=" + getInt("sort") + "&keyword=" + getString("keyword") + "'>"
                        + country.Name + "</a></td>").append("</tr>");
        x++;
    }
}
@Override
protected void process(Connection connection) throws Exception
{
    // TODO Auto-generated method stub
}
}