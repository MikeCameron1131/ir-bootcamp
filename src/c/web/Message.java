package c.web;

public class Message
{
public boolean error;
public String fieldId;
public String message;
public Message()
{
}
public Message(String FeildId, String Message, boolean Error)
{
    this.message = Message;
    this.fieldId = FeildId;
    this.error = Error;
}
@Override
public String toString()
{
    return "{\"fieldId\":\"" + fieldId + "\",\"message\":\"" + message + "\",\"error\":\"" + error
            + "\"}";
}
}