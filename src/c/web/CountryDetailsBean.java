package c.web;

import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import c.data.Country;

public class CountryDetailsBean extends BaseBean
{
public static final String Yes = "Yes";
public static final String No = "No";
public static final String DeleteForm = "Delete";
public boolean showDeleteForm;
public final Country country = new Country();
public CountryDetailsBean(HttpServletRequest request, HttpServletResponse response) throws Exception
{
    super(request, response);
    url = "country.jsp";
    beanWrap(posted());
}
@Override
protected void prepare(Connection connection) throws Exception
{
    country.Id = getInt("id");
    country.selectById(connection);
    showDeleteForm = getString("Delete").equals(DeleteForm);
    System.out.print(country.toString());
}
@Override
protected void process(Connection connection) throws Exception
{
    if (!getString("Delete").equals(No))
    {
        if (getString("Delete").equals(Yes))
        {
            if (!country.delete(connection))
            {
                connection.close();
                setRedirect();
                return;
            }
        }
        country.Name = getString("name").trim();
        if (country.validate(connection, messages))
        {
            if (country.Id == 0)
            {
                country.insert(connection);
            }
            else
            {
                country.update(connection);
            }
            if (!showDeleteForm)
            {
                setRedirect();
            }
        }
    }
}
}