package c.web;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import c.data.Country;
import c.data.Department;
import c.data.Expense;
import c.data.Person;
import c.helpers.DatabaseHelper;

public abstract class BaseBean
{
public static String url = "";
public static String getDate()
{
    @SuppressWarnings("unused")
    String returnDate = "";
    Date date = new Date();
    SimpleDateFormat format = new SimpleDateFormat("MM/dd/YYYY HH:mm a");
    return returnDate = format.format(date);
}
public Connection connection;
protected List<Message> messages = new ArrayList<Message>();
public NavigationBar nav = new NavigationBar();
protected final HttpServletRequest request;
protected final HttpServletResponse response;
public BaseBean(HttpServletRequest request, HttpServletResponse response)
{
    this.request = request;
    this.response = response;
}
public void beanWrap()
{
    try
    {
        connection = DatabaseHelper.getConnection();
        processBean();
    }
    catch (Exception exc)
    {
        System.out.println("Exception Occured at: " + getDate());
        System.out.println(exc.getMessage());
        exc.printStackTrace();
        try
        {
            response.sendRedirect("error.jsp");
        }
        catch (Exception exc2)
        {
            System.out.print("Redirect to error.jsp failed: " + exc2.getMessage());
        }
    }
    finally
    {
        try
        {
            connection.close();
        }
        catch (SQLException exc)
        {
            System.out.println("Unable to close DB connection: " + exc.getMessage());
            exc.printStackTrace();
        }
    }
}
public float getFloat(String parameterName)
{
    try
    {
        return Float.parseFloat(request.getParameter(parameterName));
    }
    catch (Exception exc)
    {
        return 0;
    }
}
public int getInt(String parameterName)
{
    try
    {
        return Integer.parseInt(request.getParameter(parameterName));
    }
    catch (Exception exc)
    {
        return 0;
    }
}
public String getMessageHtm()
{
    StringBuilder sb = new StringBuilder();
    for (Message message : messages)
    {
        if (message.error)
        {
            sb.append("<div class='alert'>" + message.message + "</div>");
        }
        else
        {
            sb.append(message.message);
        }
    }
    return sb.toString();
}
public String getMessageJson()
{
    String comma = "";
    StringBuilder sb = new StringBuilder("[");
    for (Message message : messages)
    {
        sb.append(comma).append(message);
        comma = ",";
    }
    return sb.append("]").toString();
}
public List<Message> getMessageList()
{
    return messages;
}
public String getString(String parameterName)
{
    String result = request.getParameter(parameterName);
    return result == null ? "" : result;
}
public boolean hasParameter(String parameterName)
{
    return parameterName.equals(request.getParameter(parameterName));
}
public boolean isEmptyInt(int value)
{
    if (value <= 0)
    {
        return true;
    }
    return false;
}
public boolean isEmptyString(String value)
{
    if (value.equals(null) || value.trim().length() == 0)
    {
        return true;
    }
    return false;
}
protected boolean posted()
{
    return request.getMethod().equalsIgnoreCase("POST");
}
// protected abstract void prepare(Connection connection) throws Exception;
protected abstract void processBean() throws Exception;
public void resetPage() throws Exception
{
    response.sendRedirect(url);
}
public void setRedirect() throws Exception
{
    File file = new File(url);
    if (!file.exists())
    {
        new Exception("Unable to redirect to requested resource " + url);
    }
    response.sendRedirect(url + "?sort=" + getInt("sort") + "&keyword=" + getString("keyword"));
}
public String toCountryJavaScriptArray(List<Country> list)
{
    String comma = "";
    StringBuilder sb = new StringBuilder("[");
    for (Country country : list)
    {
        sb.append(comma).append(country.toString());
        comma = ",";
    }
    return sb.append("]").toString();
}
public String toDepartmentJavaScriptArray(List<Department> list)
{
    String comma = "";
    StringBuilder sb = new StringBuilder("[");
    for (Department department : list)
    {
        sb.append(comma).append(department.toString());
        comma = ",";
    }
    return sb.append("]").toString();
}
public String toExpenseJavaScriptArray(List<Expense> list)
{
    String comma = "";
    StringBuilder sb = new StringBuilder("[");
    for (Expense expense : list)
    {
        sb.append(comma).append(expense.toString());
        comma = ",";
    }
    return sb.append("]").toString();
}
public String toPersonJavaScriptArray(List<Person> list)
{
    String comma = "";
    StringBuilder sb = new StringBuilder("[");
    for (Person person : list)
    {
        sb.append(comma).append(person.toString());
        comma = ",";
    }
    return sb.append("]").toString();
}
}