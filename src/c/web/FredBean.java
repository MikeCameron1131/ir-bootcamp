package c.web;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import c.data.Country;
import c.data.Department;
import c.data.Expense;
import c.data.Person;

public class FredBean extends BaseBean
{
public String countries;
public String departments;
public String expenses;
public String people;
public FredBean(HttpServletRequest request, HttpServletResponse response)
{
    super(request, response);
    beanWrap();
    url = "fred.jsp";
}
@Override
protected void processBean() throws Exception
{
    List<Country> countryList = Country.select(connection, getString("keyword"), getInt("sort"));
    countries = toCountryJavaScriptArray(countryList);
    List<Department> departmentList = Department.select(connection, getString("keyword"), getInt(
            "sort"));
    departments = toDepartmentJavaScriptArray(departmentList);
    List<Expense> expenseList = Expense.select(connection, getString("Keyword"), getInt("sort"));
    expenses = toExpenseJavaScriptArray(expenseList);
    List<Person> personList = Person.select(connection, getString("keyword"), getInt("sort"));
    people = toPersonJavaScriptArray(personList);
}
}