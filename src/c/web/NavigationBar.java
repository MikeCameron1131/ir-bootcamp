package c.web;

public class NavigationBar
{
public static final String Country = "country.jsp";
public static final String Person = "person.jsp";
public static final String Expense = "expense.jsp";
public static final String Department = "department.jsp";
public static final String Fred = "fred.jsp";
public static final String ActiveNav = "active";
public StringBuilder navBar(String View)
{
    String ActiveCountry = "";
    String ActivePerson = "";
    String ActiveExpense = "";
    String ActiveDepartment = "";
    String ActiveFred = "";
    StringBuilder sb = new StringBuilder();
    if (View.equals(Country))
    {
        ActiveCountry = ActiveNav;
    }
    else if (View.equals(Person))
    {
        ActivePerson = ActiveNav;
    }
    else if (View.equals(Expense))
    {
        ActiveExpense = ActiveNav;
    }
    else if (View.equals(Department))
    {
        ActiveDepartment = ActiveNav;
    }
    else if (View.equals(Fred))
    {
        ActiveFred = ActiveNav;
    }
    sb.append("<ul class='navbar'>").append("<li class='navpill " + ActivePerson
            + "'><a href='/ProjectC/person.jsp' class='navlink'>People</a></li>").append(
                    "<li class='navpill " + ActiveCountry
                            + "'><a href='/ProjectC/country.jsp' class='navlink'>Countries</a></li>")
            .append("<li class='navpill " + ActiveExpense
                    + "'><a href='/ProjectC/expense.jsp' class='navlink'>Expenses</a></li>").append(
                            "<li class='navpill " + ActiveDepartment
                                    + "'><a href='/ProjectC/department.jsp' class='navlink'>Departments</a></li>")
            .append("<li class='navpill " + ActiveFred
                    + "'><a href='fred.jsp' class='navlink'>FRED</a></li>").append("</ul>");
    return sb;
}
}
