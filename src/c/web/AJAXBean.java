package c.web;

import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import c.data.Country;
import c.data.Department;
import c.data.Expense;
import c.data.Person;

public class AJAXBean extends BaseBean
{
public static final boolean Success = true;
public static final boolean Failure = false;
public static final String Keyword = "keyword";
public static final String deletePerson = "deletePerson";
public static final String updatePerson = "updatePerson";
public static final String insertPerson = "insertPerson";
private PrintWriter writer;
public Person person = new Person();
public Country country = new Country();
public Department department = new Department();
public Expense expense = new Expense();
public AJAXBean(HttpServletRequest request, HttpServletResponse response)
{
    super(request, response);
    beanWrap();
}
public String buildResponse(Boolean Success, Object obj) throws Exception
{
    StringBuilder sb = new StringBuilder();
    String comma = "";
    if (Success)
    {
        sb.append("{\"Ok\":\"1\",\"obj\":" + obj.toString() + "}");
        return sb.toString();
    }
    else
    {
        sb.append("{\"Ok\":\"0\",\"ee\":[");
        for (Message message : messages)
        {
            sb.append(comma).append(message);
            comma = ",";
        }
        return sb.append("]}").toString();
    }
}
public String deleteCountry() throws Exception
{
    country.Id = getInt("Id");
    if (!country.delete(connection))
    {
        return buildResponse(Success, country);
    }
    return buildResponse(Failure, country);
}
public String deleteDepartment() throws Exception
{
    department.Id = getInt("Id");
    if (!department.delete(connection))
    {
        return buildResponse(Success, department);
    }
    return buildResponse(Failure, department);
}
public String deleteExpense() throws Exception
{
    expense.Id = getInt("Id");
    if (!expense.delete(connection))
    {
        return buildResponse(Success, expense);
    }
    return buildResponse(Failure, expense);
}
public String deletePerson() throws Exception
{
    person.Id = getInt("Id");
    if (!person.delete(connection))
    {
        return buildResponse(Success, person);
    }
    return buildResponse(Success, person);
}
public String insertCountry() throws Exception
{
    country.Name = getString("Name");
    if (country.validate(connection, messages))
    {
        country.Id = country.insert(connection);
        country.selectById(connection);
        return buildResponse(Success, country);
    }
    return buildResponse(Failure, country);
}
public String insertDepartment() throws Exception
{
    department.Name = getString("Name");
    department.Manager = getInt("Manager");
    if (department.validate(connection, messages))
    {
        department.Id = department.insert(connection);
        department.selectById(connection);
        return buildResponse(Success, department);
    }
    return buildResponse(Failure, country);
}
public String insertExpense() throws Exception
{
    expense.Description = getString("Description");
    expense.Amount = getFloat("Amount");
    expense.Payer = getInt("Payer");
    if (expense.validate(connection, messages))
    {
        expense.Id = expense.insert(connection);
        expense.selectById(connection);
        return buildResponse(Success, expense);
    }
    return buildResponse(Failure, expense);
}
public String insertPerson() throws Exception
{
    person.FirstName = getString("FirstName");
    person.PhoneNumber = getInt("PhoneNumber");
    if (person.validate(messages, connection))
    {
        person.Id = person.insert(connection);
        person.selectById(connection);
        return buildResponse(Success, person);
    }
    return buildResponse(Failure, person);
}
@Override
protected void processBean() throws Exception
{
    String fn = getString("fn");
    Class<?> params[] = {};
    Method method = this.getClass().getMethod(fn, params);
    writer = response.getWriter();
    writer.println(method.invoke(this, null));
}
public String selectCountries() throws Exception
{
    List<Country> countryList = Country.select(connection, getString("keyword"), getInt("sort"));
    StringBuilder countries = new StringBuilder("{\"Ok\":1,\"countries\":");
    countries.append(toCountryJavaScriptArray(countryList));
    countries.append("}");
    return countries.toString();
}
public String selectDepartments() throws Exception
{
    List<Department> departmentList = Department.select(connection, getString("keyword"), getInt(
            "sort"));
    StringBuilder departments = new StringBuilder("{\"Ok\":1,\"departments\":");
    departments.append(toDepartmentJavaScriptArray(departmentList));
    departments.append("}");
    return departments.toString();
}
public String selectExpenses() throws Exception
{
    List<Expense> expenseList = Expense.select(connection, getString("keyword"), getInt("sort"));
    StringBuilder expenses = new StringBuilder("{\"Ok\":1,\"expenses\":");
    expenses.append(toExpenseJavaScriptArray(expenseList));
    expenses.append("}");
    return expenses.toString();
}
public String selectPeople() throws Exception
{
    List<Person> personList = Person.select(connection, getString("keyword"), getInt("sort"));
    StringBuilder people = new StringBuilder("{\"Ok\":1,\"people\":");
    people.append(toPersonJavaScriptArray(personList));
    people.append("}");
    return people.toString();
}
public String updateCountry() throws Exception
{
    country.Id = getInt("Id");
    country.selectById(connection);
    country.Name = getString("Name");
    if (country.validate(connection, messages))
    {
        country.update(connection);
        return buildResponse(Success, country);
    }
    return buildResponse(Failure, country);
}
public String updateDepartment() throws Exception
{
    department.Id = getInt("Id");
    department.selectById(connection);
    department.Name = getString("Name");
    department.Manager = getInt("Manager");
    if (department.validate(connection, messages))
    {
        department.update(connection);
        return buildResponse(Success, department);
    }
    return buildResponse(Failure, department);
}
public String updateExpense() throws Exception
{
    expense.Id = getInt("Id");
    expense.selectById(connection);
    expense.Description = getString("Description");
    expense.Amount = getFloat("Amount");
    expense.Payer = getInt("Payer");
    if (expense.validate(connection, messages))
    {
        expense.update(connection);
        return buildResponse(Success, expense);
    }
    return buildResponse(Failure, expense);
}
public String updatePerson() throws Exception
{
    person.Id = getInt("Id");
    person.selectById(connection);
    person.FirstName = getString("FirstName");
    person.PhoneNumber = getInt("PhoneNumber");
    if (person.validate(messages, connection))
    {
        person.update(connection);
        return buildResponse(Success, person);
    }
    return buildResponse(Failure, person);
}
}
