package c.web;

import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import c.data.Department;

public class DepartmentDetailsBean extends BaseBean
{
public static final String Yes = "Yes";
public static final String No = "No";
public static final String DeleteForm = "Delete";
public boolean showDeleteForm;
public final Department department = new Department();
public StringBuilder dropdown = new StringBuilder();
public DepartmentDetailsBean(HttpServletRequest request, HttpServletResponse response)
        throws Exception
{
    super(request, response);
    url = "department.jsp";
    showDeleteForm = getString("Delete").equals(DeleteForm);
    beanWrap(posted());
}
@Override
protected void prepare(Connection connection) throws Exception
{
    department.Id = getInt("id");
    department.selectById(connection);
    dropdown = PersonDropDown.personDropDown(connection, department.Manager);
}
@Override
protected void process(Connection connection) throws Exception
{
    if (!getString("Delete").equals(No))
    {
        if (getString("Delete").equals(Yes))
        {
            if (!department.delete(connection))
            {
                setRedirect();
                return;
            }
        }
        department.Name = getString("name");
        department.Manager = getInt("manager");
        dropdown = PersonDropDown.personDropDown(connection, department.Manager);
        if (department.validate(connection, messages))
        {
            if (department.Id == 0)
            {
                department.insert(connection);
            }
            else
            {
                department.update(connection);
            }
            if (!showDeleteForm)
            {
                setRedirect();
            }
        }
    }
}
}