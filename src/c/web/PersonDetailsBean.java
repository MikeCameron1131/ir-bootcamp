package c.web;

import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import c.data.Person;

public class PersonDetailsBean extends BaseBean
{
public static final String Yes = "Yes";
public static final String No = "No";
public Person person = new Person();
public final String DeleteForm = "Delete";
public boolean showDeleteForm;
public PersonDetailsBean(HttpServletRequest request, HttpServletResponse response) throws Exception
{
    super(request, response);
    url = "person.jsp";
    showDeleteForm = getString("Delete").equals(DeleteForm);
    beanWrap(posted());
}
@Override
protected void prepare(Connection connection) throws Exception
{
    person.Id = getInt("id");
    person.selectById(connection);
}
@Override
protected void process(Connection connection) throws Exception
{
    if (!getString("Delete").equals(No))
    {
        if (getString("Delete").equals(Yes))
        {
            if (!person.delete(connection))
            {
                setRedirect();
                return;
            }
        }
        person.FirstName = getString("name");
        person.PhoneNumber = getInt("phone");
        if (person.validate(messages, connection))
        {
            if (person.Id == 0)
            {
                person.insert(connection);
            }
            else
            {
                person.update(connection);
            }
            if (!showDeleteForm)
            {
                setRedirect();
            }
        }
    }
}
}