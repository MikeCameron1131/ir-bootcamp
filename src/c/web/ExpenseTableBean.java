package c.web;

import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import c.data.Expense;
import c.data.Person;

public class ExpenseTableBean extends BaseBean
{
public static final String pageType = "Expense";
public static StringBuilder sb = new StringBuilder();
public String keyword = getString("keyword");
public int sort = getInt("sort");
public SearchByForm form = new SearchByForm();
private final DecimalFormat df = new DecimalFormat("0.00");
public ExpenseTableBean(HttpServletRequest request, HttpServletResponse response)
{
    super(request, response);
    url = "expense.jsp";
    beanWrap(false);
}
@Override
protected void prepare(Connection connection) throws Exception
{
    sb = new StringBuilder();
    if (getString("reset").equals("Reset"))
    {
        resetPage();
        return;
    }
    List<Expense> list = Expense.select(connection, keyword, sort);
    int x = 0;
    Person person = new Person();
    for (Expense expense : list)
    {
        person.Id = expense.Payer;
        person.selectById(connection);
        String name = person.Id == 0 ? "None" : person.FirstName;
        sb.append("<tr");
        if ((x % 2) == 0)
        {
            sb.append(" class='trGreen'");
        }
        sb.append(">").append("<td class='rightAlign'>" + expense.Id + "</td>").append(
                "<td class='leftAlign'><a href='../ProjectC/expensedtl.jsp?id=" + expense.Id
                        + "&sort=" + sort + "&keyword=" + keyword + "'>" + expense.Description
                        + "</a></td>").append("<td class='rightAlign'>" + df.format(expense.Amount)
                                + "</td>").append("<td class='leftAlign'>" + name + "</td>").append(
                                        "</tr>");
        x++;
    }
}
@Override
protected void process(Connection connection) throws Exception
{
    // TODO Auto-generated method stub
}
}
