package c.web;

import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import c.data.Expense;

public class ExpenseDetailsBean extends BaseBean
{
public static final String Yes = "Yes";
public static final String No = "No";
public Expense expense = new Expense();
public final String DeleteForm = "Delete";
public boolean showDeleteForm;
public StringBuilder dropdown = new StringBuilder();
public ExpenseDetailsBean(HttpServletRequest request, HttpServletResponse response) throws Exception
{
    super(request, response);
    url = "expense.jsp";
    showDeleteForm = getString("Delete").equals(DeleteForm);
    beanWrap(posted());
}
@Override
protected void prepare(Connection connection) throws Exception
{
    expense.Id = getInt("id");
    expense.selectById(connection);
    dropdown = PersonDropDown.personDropDown(connection, expense.Payer);
    System.out.println(expense.toString());
}
@Override
protected void process(Connection connection) throws Exception
{
    if (!getString("Delete").equals(No))
    {
        if (getString("Delete").equals(Yes))
        {
            if (!expense.delete(connection))
            {
                setRedirect();
                return;
            }
        }
        expense.Description = getString("description").trim();
        expense.Amount = getFloat("amount");
        expense.Payer = getInt("payer");
        dropdown = PersonDropDown.personDropDown(connection, expense.Payer);
        if (expense.validate(connection, messages))
        {
            if (expense.Id == 0)
            {
                expense.insert(connection);
            }
            else
            {
                expense.update(connection);
            }
            if (!showDeleteForm)
            {
                setRedirect();
            }
        }
    }
}
}