package c.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import c.helpers.QueryHelper;
import c.web.Message;

public class Expense extends QueryHelper
{
public static final String AmountField = "amount";
public static final String Blank = "";
public static final String DescriptionField = "description";
public static final boolean Error = true;
public static final String PayerField = "payer";
public static final int SortByAmount = 3;
public static final int SortByDescription = 2;
public static final int SortByPayer = 4;
public static final int SortByPerson = 2;
public static final String Table = "expenses";
public static List<Expense> select(Connection connection, String nameContains, int sortBy)
        throws Exception
{
    List<Expense> result = new ArrayList<Expense>();
    StringBuilder queryString = new StringBuilder();
    queryString.append(
            "SELECT e.id, e.description, e.amount, e.payer, p.firstname FROM expenses e LEFT JOIN people p on e.payer=p.id");
    if (nameContains.trim().length() != 0)
    {
        queryString.append(" WHERE e.description LIKE ?");
    }
    if (sortBy == SortByDescription)
    {
        queryString.append(" ORDER BY e.description");
    }
    else if (sortBy == SortByAmount)
    {
        queryString.append(" ORDER BY e.amount");
    }
    else if (sortBy == SortByPayer)
    {
        queryString.append(" ORDER BY p.firstname, e.description");
    }
    else
    {
        queryString.append(" ORDER BY id");
    }
    String query = queryString.toString();
    try (PreparedStatement statement = connection.prepareStatement(query);)
    {
        if (nameContains.trim().length() != 0)
        {
            String userInput = "%" + nameContains + "%";
            statement.setString(1, userInput);
        }
        try (ResultSet rs = statement.executeQuery();)
        {
            while (rs.next())
            {
                Expense expense = new Expense();
                expense.populate(rs);
                result.add(expense);
            }
        }
    }
    return result;
}
public Float Amount;
public String Description;
public int Id;
/** Mandatory reference to person.id */
public int Payer;
public Expense()
{
}
public boolean delete(Connection connection) throws Exception
{
    return this.deleteObject(connection, Table);
}
public int insert(Connection connection) throws Exception
{
    this.Id = this.insertObject(connection, "Insert INTO " + Table
            + " (description, amount, payer) Values(?,?,?)", this.Description, this.Amount,
            this.Payer);
    return this.Id;
}
public boolean isDuplicateDescription(Connection connection) throws Exception
{
    return this.isDuplicate(connection, "SELECT COUNT(*) FROM " + Table
            + " WHERE description=? and id<>?", this.Description, this.Id) > 0;
}
public void populate(ResultSet rs) throws Exception
{
    this.Id = rs.getInt("id");
    this.Description = rs.getString("description");
    this.Amount = rs.getFloat("amount");
    this.Payer = rs.getInt("payer");
}
public void selectById(Connection connection) throws Exception
{
    String query = "SELECT * FROM " + Table + " WHERE id=?";
    try (PreparedStatement statement = connection.prepareStatement(query);)
    {
        statement.setInt(1, this.Id);
        try (ResultSet rs = statement.executeQuery();)
        {
            if (rs.first())
            {
                populate(rs);
            }
        }
    }
}
@Override
public String toString()
{
    return "{\"Id\":" + this.Id + ",\"Description\":\"" + this.Description + "\",\"Amount\":"
            + this.Amount + ",\"Payer\": " + this.Payer + "}";
}
public boolean update(Connection connection) throws Exception
{
    return this.updateObject(connection, "UPDATE " + Table
            + " SET description=?, amount=?, payer=? WHERE id=?", this.Description, this.Amount,
            this.Payer, this.Id);
}
public boolean validate(Connection connection, List<Message> writeHere) throws Exception
{
    if (this.isDuplicateDescription(connection))
    {
        writeHere.add(new Message(DescriptionField, "  Description Already Assigned", Error));
    }
    if (this.Description.trim().length() == 0)
    {
        writeHere.add(new Message(DescriptionField, "Please Enter a Description", Error));
    }
    if (this.Amount <= 0)
    {
        writeHere.add(new Message(AmountField, "Please Enter an Amount", Error));
    }
    if (this.Payer <= 0)
    {
        writeHere.add(new Message(PayerField, "Please Enter a Payer", Error));
    }
    return writeHere.size() == 0;
}
}