package c.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import c.helpers.QueryHelper;
import c.web.Message;

public class Country extends QueryHelper
{
public static final boolean Error = true;
public static final String NameField = "name";
public static final int SortById = 1;
public static final String Blank = "";
public static final String Table = "countries";
public final static int SortByName = 2;
public static List<Country> select(Connection connection, String nameContains, int sortBy)
        throws Exception
{
    List<Country> result = new ArrayList<Country>();
    StringBuilder queryString = new StringBuilder();
    queryString.append("SELECT * FROM " + Table);
    if (nameContains.trim().length() != 0)
    {
        queryString.append(" WHERE name LIKE ?");
    }
    if (sortBy == SortByName)
    {
        queryString.append(" ORDER BY name");
    }
    else
    {
        queryString.append(" ORDER BY id");
    }
    String query = queryString.toString();
    try (PreparedStatement statement = connection.prepareStatement(query);)
    {
        if (nameContains.trim().length() != 0)
        {
            String userInput = "%" + nameContains + "%";
            statement.setString(1, userInput);
        }
        try (ResultSet rs = statement.executeQuery();)
        {
            while (rs.next())
            {
                Country country = new Country();
                country.populate(rs);
                result.add(country);
            }
        }
    }
    return result;
}
public int Id;
public String Name;
public Country()
{
}
public boolean delete(Connection connection) throws Exception
{
    return this.deleteObject(connection, Table);
}
public int insert(Connection connection) throws Exception
{
    this.Id = this.insertObject(connection, "Insert INTO " + Table + " (name) Values(?)",
            this.Name);
    return this.Id;
}
public boolean isDuplicateName(Connection connection) throws Exception
{
    return this.isDuplicate(connection, "SELECT COUNT(*) FROM " + Table + " WHERE name=? and id<>?",
            this.Name, this.Id) > 0;
}
public void populate(ResultSet rs) throws Exception
{
    this.Id = rs.getInt("id");
    this.Name = rs.getString("name");
}
public void selectById(Connection connection) throws Exception
{
    String query = "SELECT * FROM " + Table + " WHERE id=?";
    try (PreparedStatement statement = connection.prepareStatement(query);)
    {
        statement.setInt(1, this.Id);
        try (ResultSet rs = statement.executeQuery();)
        {
            if (rs.first())
            {
                populate(rs);
            }
        }
    }
}
@Override
public String toString()
{
    return "{\"Id\":" + this.Id + ",\"Name\":\"" + this.Name + "\"}";
}
public boolean update(Connection connection) throws Exception
{
    return this.updateObject(connection, "UPDATE " + Table + " SET name=? WHERE id=?", this.Name,
            this.Id);
}
public boolean validate(Connection connection, List<Message> writeHere) throws Exception
{
    if (this.isDuplicateName(connection))
    {
        writeHere.add(new Message(NameField, "Name Already Assigned", Error));
    }
    else if (this.Name.trim().length() == 0)
    {
        writeHere.add(new Message(NameField, "Please Enter a Name", Error));
    }
    return writeHere.size() == 0;
}
}