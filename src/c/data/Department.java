package c.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import c.helpers.QueryHelper;
import c.web.Message;

public class Department extends QueryHelper
{
public static final String Blank = "";
public static final boolean Error = true;
public static final String NameField = "name";
public static final int SortByManager = 3;
public static final int SortByName = 2;
public static final String Table = "departments";
public static List<Department> select(Connection connection, String nameContains, int sortBy)
        throws Exception
{
    List<Department> result = new ArrayList<Department>();
    StringBuilder queryString = new StringBuilder(
            "SELECT d.id, d.name, d.manager, p.firstname FROM departments d LEFT JOIN people p on d.manager=p.id where 1=1");
    if (nameContains.trim().length() != 0)
    {
        queryString.append(" WHERE name LIKE ?");
    }
    if (sortBy == SortByName)
    {
        queryString.append(" ORDER BY d.name");
    }
    else if (sortBy == SortByManager)
    {
        queryString.append(" ORDER BY p.firstname, d.name");
    }
    else
    {
        queryString.append(" ORDER BY id");
    }
    String query = queryString.toString();
    try (PreparedStatement statement = connection.prepareStatement(query);)
    {
        if (nameContains.trim().length() != 0)
        {
            String userInput = "%" + nameContains + "%";
            statement.setString(1, userInput);
        }
        try (ResultSet rs = statement.executeQuery();)
        {
            while (rs.next())
            {
                Department department = new Department();
                department.populate(rs);
                result.add(department);
            }
        }
    }
    return result;
}
public int Id = 0;
public int Manager;
public String Name = "";
public Department()
{
}
public boolean delete(Connection connection) throws Exception
{
    return this.deleteObject(connection, Table);
}
public int insert(Connection connection) throws Exception
{
    this.Id = this.insertObject(connection, "Insert INTO " + Table + " (name, manager) Values(?,?)",
            this.Name, this.Manager);
    return this.Id;
}
public boolean isDuplicateName(Connection connection) throws Exception
{
    return this.isDuplicate(connection, "SELECT COUNT(*) FROM " + Table + " WHERE name=? and id<>?",
            this.Name, this.Id) > 0;
}
public void populate(ResultSet rs) throws Exception
{
    this.Id = rs.getInt("id");
    this.Name = rs.getString("name");
    this.Manager = rs.getInt("manager");
}
public void selectById(Connection connection) throws Exception
{
    String query = "SELECT * FROM " + Table + " WHERE id=?";
    try (PreparedStatement statement = connection.prepareStatement(query);)
    {
        statement.setInt(1, this.Id);
        try (ResultSet rs = statement.executeQuery();)
        {
            if (rs.first())
            {
                populate(rs);
            }
        }
    }
}
@Override
public String toString()
{
    return "{\"Id\":" + this.Id + ",\"Manager\":" + this.Manager + ",\"Name\":\"" + this.Name
            + "\"}";
}
public boolean update(Connection connection) throws Exception
{
    return this.updateObject(connection, "UPDATE " + Table + " SET name=?, manager=? WHERE id=?",
            this.Name, this.Manager, this.Id);
}
public boolean validate(Connection connection, List<Message> writeHere) throws Exception
{
    if (this.isDuplicateName(connection))
    {
        writeHere.add(new Message(NameField, "Name Already Assigned", Error));
    }
    if (this.Name.trim().length() == 0)
    {
        writeHere.add(new Message(NameField, "Please Enter Name", Error));
    }
    return writeHere.size() == 0;
}
}