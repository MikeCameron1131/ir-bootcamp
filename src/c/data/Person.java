package c.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import c.helpers.QueryHelper;
import c.web.Message;

public class Person extends QueryHelper
{
public static final boolean Error = true;
public static final String NameField = "name";
public static final String PhoneField = "phone";
public static final int SortById = 1;
public static final int SortByName = 2;
public static final int SortByPhone = 3;
public static final String Table = "people";
public static List<Person> select(Connection connection, String nameContains, int sortBy)
        throws Exception
{
    List<Person> result = new ArrayList<Person>();
    StringBuilder queryString = new StringBuilder();
    queryString.append("SELECT * FROM " + Table);
    if (nameContains.trim().length() != 0)
    {
        queryString.append(" WHERE firstname LIKE ?");
    }
    if (sortBy == SortByName)
    {
        queryString.append(" ORDER BY firstname");
    }
    else if (sortBy == SortByPhone)
    {
        queryString.append(" ORDER BY phonenumber");
    }
    else
    {
        queryString.append(" ORDER BY id");
    }
    String query = queryString.toString();
    try (PreparedStatement statement = connection.prepareStatement(query);)
    {
        if (nameContains.trim().length() != 0)
        {
            String userInput = "%" + nameContains + "%";
            statement.setString(1, userInput);
        }
        try (ResultSet rs = statement.executeQuery();)
        {
            while (rs.next())
            {
                Person person = new Person();
                person.populate(rs);
                result.add(person);
            }
        }
    }
    return result;
}
public String FirstName = "";
public int Id;
public int PhoneNumber = 0;
public Person()
{
}
public boolean delete(Connection connection) throws Exception
{
    return this.deleteObject(connection, Table);
}
public int insert(Connection connection) throws Exception
{
    this.Id = this.insertObject(connection, "Insert INTO " + Table
            + " (firstname,phonenumber) Values(?,?)", this.FirstName, this.PhoneNumber);
    return this.Id;
}
public boolean isDuplicateName(Connection connection) throws Exception
{
    return this.isDuplicate(connection, "SELECT COUNT(*) FROM " + Table
            + " WHERE firstname=? and id<>?", this.FirstName, this.Id) > 0;
}
public boolean isDuplicatePhone(Connection connection) throws Exception
{
    return this.isDuplicate(connection, "SELECT COUNT(*) FROM " + Table
            + " WHERE phonenumber=? and id<>?", this.PhoneNumber, this.Id) > 0;
}
public void populate(ResultSet rs) throws Exception
{
    this.Id = rs.getInt("id");
    this.FirstName = rs.getString("firstname");
    this.PhoneNumber = rs.getInt("phonenumber");
}
public void selectById(Connection connection) throws Exception
{
    String query = "SELECT * FROM " + Table + " WHERE id=?";
    try (PreparedStatement statement = connection.prepareStatement(query);)
    {
        statement.setInt(1, this.Id);
        try (ResultSet rs = statement.executeQuery();)
        {
            if (rs.first())
            {
                populate(rs);
            }
            else
            {
                this.Id = 0;
            }
        }
    }
}
@Override
public String toString()
{
    return "{\"Id\":" + this.Id + ",\"FirstName\":\"" + this.FirstName + "\",\"PhoneNumber\":"
            + this.PhoneNumber + "}";
}
public boolean update(Connection connection) throws Exception
{
    return this.updateObject(connection, "UPDATE " + Table
            + " SET firstname=?, phonenumber=? WHERE id=?", this.FirstName, this.PhoneNumber,
            this.Id);
}
public boolean validate(List<Message> writeHere, Connection connection) throws Exception
{
    if (this.isDuplicateName(connection))
    {
        writeHere.add(new Message(NameField, "Name Already Assigned", Error));
    }
    if (this.FirstName.trim().length() == 0)
    {
        writeHere.add(new Message(NameField, "Please Enter a Name", Error));
    }
    if (this.isDuplicatePhone(connection))
    {
        writeHere.add(new Message(PhoneField, "Phone Number Already Assigned", Error));
    }
    if (this.PhoneNumber <= 0)
    {
        writeHere.add(new Message(PhoneField, "Please Enter a Phone Number", Error));
    }
    return writeHere.size() == 0;
}
}