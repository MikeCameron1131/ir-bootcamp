package c.helpers;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class QueryHelper
{
public boolean deleteObject(Connection connection, String Table) throws Exception
{
    String query = "DELETE FROM " + Table + " WHERE id=?";
    try (PreparedStatement statement = connection.prepareStatement(query))
    {
        Field field = this.getClass().getField("Id");
        statement.setObject(1, field.get(this));
        return statement.execute();
    }
}
public int insertObject(Connection connection, String query, Object... params) throws Exception
{
    try (PreparedStatement statement = connection.prepareStatement(query,
            Statement.RETURN_GENERATED_KEYS);)
    {
        for (int i = 0; i < params.length; i++)
        {
            statement.setObject(i + 1, params[i]);
        }
        statement.execute();
        try (ResultSet rs = statement.getGeneratedKeys();)
        {
            if (rs.next())
            {
                return rs.getInt(1);
            }
        }
    }
    return 0;
}
public int isDuplicate(Connection connection, String query, Object... params) throws Exception
{
    try (PreparedStatement statement = connection.prepareStatement(query);)
    {
        for (int i = 0; i < params.length; i++)
        {
            statement.setObject(i + 1, params[i]);
        }
        try (ResultSet rs = statement.executeQuery();)
        {
            while (rs.next())
            {
                return rs.getInt(1);
            }
        }
    }
    return 0;
}
public boolean updateObject(Connection connection, String query, Object... params) throws Exception
{
    try (PreparedStatement statement = connection.prepareStatement(query);)
    {
        for (int i = 0; i < params.length; i++)
        {
            statement.setObject(i + 1, params[i]);
        }
        return statement.execute();
    }
}
}