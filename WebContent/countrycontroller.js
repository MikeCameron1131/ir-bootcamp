//
"use strict";
//
var countrycontroller = (function() {
    var self = {
	prefix : "Country",
	button : function(type) {
	    var button = mc.get("navButton");
	    if (type === "Add") {
		button.innerHTML = "Add " + self.prefix;
		button.onclick = function() {
		    self.edit(0);
		};
	    } else {
		button.innerHTML = "Return"
		button.onclick = function() {
		    self.button("Add");
		    view.displayList(self.prefix + "List");
		};
	    }
	},
	edit : function(countryId) {
	    var country = Country.select(countryId);
	    if (countryId != 0) {
		mc.removeClassAttribute(self.prefix + "DeleteButton",
			"noDisplay");
	    } else {
		mc.displayNone(self.prefix + "DeleteButton");
	    }
	    mc.get(self.prefix + "Id").value = country.Id;
	    mc.get(self.prefix + "Name").value = country.Name;
	    countrycontroller.button("Return");
	    view.showDetails(self.prefix + "Details");
	},
	fillList : function() {
	    if (mc.get(self.prefix + "Table").rows.length > 1) {
		mc.clearTable(self.prefix + "Table");
	    }
	    Country.sort(Country.SortBy);
	    view.pagination(Country, countrycontroller);
	    if ((Country.Position + view.pageSize) > Country.Array.length) {
		var end = Country.Array.length;
	    } else {
		var end = Country.Position + view.pageSize;
	    }
	    for (var i = Country.Position, x = 1; i < end; i++, x++) {
		var row = mc.get(self.prefix + "Table").insertRow(x);
		var country = Country.Array[i];
		row.id = "row" + country.Id;
		row.innerHTML = self.makeRow(country);
	    }
	    view.paginationStatus((Country.Position + 1), end, self.prefix
		    + "Table");
	    view.searchBar(Country, countrycontroller, "Name");
	    view.setTableRowColour(self.prefix + "Table");
	    self.button("Add");
	    view.displayList(self.prefix + "List");
	},
	insertCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		var country = Country.wrap(responseObject.obj);
		country.insert();
		var row = mc.get(self.prefix + "Table").insertRow();
		row.id = "row" + country.Id;
		row.innerHTML = self.makeRow(country);
		view.setTableRowColour(self.prefix + "Table");
		self.button("Add");
		view.displayList(self.prefix + "List");
	    } else {
		view.messagesDisplay(responseObject.ee);
	    }
	},
	makeRow : function(country) {
	    country = Country.wrap(country);
	    return "<td class='rightAlign' id='CountryIdCol"
		    + country.Id
		    + "'>"
		    + country.Id
		    + "</td><td class='leftAlign' id='CountryNameCol"
		    + country.Id
		    + "'>"
		    + view.makeLink("countrycontroller.edit", country.Id,
			    country.Name) + "</td>";
	},
	remove : function() {
	    var requestObject = {
		fn : "deleteCountry",
		Id : mc.valueOf(self.prefix + "Id"),
	    };
	    mc.ajax(requestObject, self.removeCallback);
	},
	removeCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		var country = Country.wrap(requestObject);
		country.remove();
		mc.removeRow(self.prefix + "Table", country.Id);
		country = Country.wrap(Country.Array[(Country.Array
			+ view.pageSize - 1)]);
		view.setTableRowColour(self.prefix + "Table");
		self.button("Add");
		view.displayList(self.prefix + "List");
	    } else {
		mc.get("messages").innerHTML = "Unable to delete this person. Please try again.";
	    }
	},
	reset : function() {
	    mc.get("keyword").value = "";
	    var requestObject = {
		fn : "selectCountries",
		sort : 1
	    };
	    mc.ajax(requestObject, self.resetCallback);
	},
	resetCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		model.countries = responseObject.countries;
		self.fillList();
	    }
	},
	update : function() {
	    if (mc.valueOf(self.prefix + "Id") == 0) {
		var serverFunction = "insertCountry";
		var callBack = countrycontroller.insertCallback;
	    } else {
		var serverFunction = "updateCountry";
		var callBack = countrycontroller.updateCallback
	    }
	    var requestObject = {
		fn : serverFunction,
		Id : mc.valueOf(self.prefix + "Id"),
		Name : mc.valueOf(self.prefix + "Name"),
	    };
	    mc.ajax(requestObject, callBack);
	},
	updateCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		var country = Country.wrap(responseObject.obj)
		country.update();
		mc.get("row" + country.Id).innerHTML = self.makeRow(country);
		view.setTableRowColour(self.prefix + "Table");
		self.button("Add");
		view.displayList(self.prefix + "List");
	    } else {
		view.messagesDisplay(responseObject.ee);
	    }
	},
	zz_country : 1
    };
    return self;
})();