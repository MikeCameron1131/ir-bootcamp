<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="c.web.CountryTableBean" %>
<%CountryTableBean bean = new CountryTableBean(request, response); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../ProjectC/css/style.css" rel="stylesheet" type="text/css">
<title>Countries</title>
</head>
<body>
	<%=bean.nav.navBar(bean.url)%>
	<%=bean.form.buildSearch(bean.pageType, bean.keyword, bean.sort)%>
	<form action="country.jsp" method="post">
	<input type="hidden" name="keyword" value="<%=bean.getString("keyword")%>"/>
	<table>
		<tr>
	   		<th class="rightAlign"><button type="submit" name="sort" value="1" class="btnLink">ID</button></th>
	    	<th class="leftAlign"><button type="submit" name="sort" value="2" class="btnLink">Country</button></th>
	    </tr>	    
		<%=bean.sb %>
	</table>
	</form>
	<br>
	<a href = "countrydtl.jsp?id=0&sort=<%=bean.getInt("sort")%>&keyword=<%=bean.getString("keyword") %>" class="btn addBtn btnCenter">Add New Country</a>
</body>
</html>