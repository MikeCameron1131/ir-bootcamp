<%@ page language="java" contentType="text/html; utf-8"
    pageEncoding="utf-8"%>
<%@ page import="c.web.NavigationBar" %>
<%NavigationBar nav = new NavigationBar();%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; utf-8">
<title>Project C Index</title>
</head>
<body>
	<%=nav.navBar("index") %>
	<H3>Here are a list of pages in this project</H3>
	<a href = "country.jsp">Country</a><br>
	<a href= "person.jsp">Person</a><br>
	<a href= "expense.jsp">Expense</a><br>
	<a href= "department.jsp">Departments</a>
</body>
</html>