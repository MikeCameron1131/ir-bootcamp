//
"use strict";
//
var updateRequest = "Update";
var deleteRequest = "Delete";
var mc = {
    /**
     * Invokes ajax.jsp with fn=requestObject.fn and other attributes as
     * parameters. Upon callback, invokes callbackFunction passed with response
     * parsed to an object.
     */
    ajax : function(requestObject, callbackFunction) {
	var url = "ajax.jsp?"
	for ( var param in requestObject) {
	    url += param + "=" + requestObject[param] + "&"
	}
	url = url.slice("&", -1);
	try {
	    var xmlHttpRequest = new XMLHttpRequest();
	    xmlHttpRequest.open("GET", url, true);
	    xmlHttpRequest.send();
	    xmlHttpRequest.onreadystatechange = function() {
		if (xmlHttpRequest.readyState == 4
			&& xmlHttpRequest.status == 200) {
		    var responseObject = JSON.parse(xmlHttpRequest.response);
		    callbackFunction(requestObject, responseObject);
		}
	    }
	} catch (e) {
	    console.log("Unable to process ajax request");
	    console.log(e);
	}
    },
    clearTable : function(table) {
	var length = mc.get(table).rows.length;
	while (length > 1) {
	    mc.get(table).deleteRow(length - 1);
	    length--;
	}
    },
    countByClass : function(className) {
	return document.getElementsByClassName(className);
    },
    disable : function(fieldId) {
	var element = mc.get(fieldId);
	element.setAttribute("disabled", "disabled");
	if (!element.hasAttribute("disabled") || element === null) {
	    console
		    .log("mc.disable failed: Unable to assign disabled attribute to "
			    + fieldId);
	}
	return element;
    },
    displayNone : function(fieldId) {
	var element = mc.get(fieldId)
	element.classList.add("noDisplay");
	if (!element.classList.contains("noDisplay") || element === null) {
	    console
		    .log("mc.displayNone failed: Unable to assign the noDisplay class to "
			    + fieldId);
	}
	return element;
    },
    focus : function(fieldId) {
	var element = mc.get(fieldId);
	element.focus();
	if (document.activeElement != element) {
	    console.log("mc.focus failed: Unable to focus on: " + fieldId)
	}
	return element;
    },
    get : function(fieldId) {
	var element = document.getElementById(fieldId);
	if (element == null) {
	    console.log("mc.get failed: Unable to find page element with id "
		    + fieldId);
	}
	return element;
    },
    getArrayElement : function(array, property, value) {
	for (var i = 0; i < array.length; i++) {
	    if (array[i][property] == value) {
		return array[i];
	    }
	}
	return false;
    },
    getArrayIndex : function(array, property, value) {
	for (var i = 0; i < array.length; i++) {
	    if (array[i][property] == value) {
		return i;
	    }
	}
    },
    hideAll : function(array) {
	for (var i = 0; i < array.length; i++) {
	    mc.displayNone(array[i]);
	}
    },
    removeClassAttribute : function(fieldId, classAttribute) {
	var element = mc.get(fieldId);
	element.classList.remove(classAttribute);
	if (element.classList.contains(classAttribute)) {
	    console
		    .log("mc.removeClassAttribute failed: Unable to remove class attribute"
			    + classAttribute + " from +" + fieldId);
	}
	return element;
    },
    removeFromArray : function(array, property, value) {
	array.forEach(function(result, index) {
	    if (result[property] == value) {
		array.splice(index, 1);
	    }
	});
	return array;
    },
    removeRow : function(table, id) {
	var element = mc.get(table);
	element.deleteRow(mc.get("row" + id).rowIndex);
    },
    setAlert : function(fieldId) {
	var element = mc.get(fieldId + "label")
	if (element.classList.add("alert")) {
	    console.log("mc.alert failed: Unable to assign the alert class to "
		    + fieldId);
	}
	return element;
    },
    valueOf : function(fieldId) {
	return mc.get(fieldId).value;
    },
};