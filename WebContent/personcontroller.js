//
"use strict";
//
var personcontroller = (function() {
    var self = {
	prefix : "person",
	button : function(type) {
	    var button = mc.get("navButton");
	    if (type === "Add") {
		button.innerHTML = "Add Person"
		button.onclick = function() {
		    self.edit(0);
		};
	    } else {
		button.innerHTML = "Return"
		button.onclick = function() {
		    self.button("Add");
		    view.displayList(self.prefix + "List");
		};
	    }
	},
	edit : function(personId) {
	    var person = Person.select(personId);
	    if (personId != 0) {
		mc.removeClassAttribute(self.prefix + "DeleteButton",
			"noDisplay");
	    } else {
		mc.displayNone(self.prefix + "DeleteButton");
	    }
	    mc.get(self.prefix + "Id").value = person.Id;
	    mc.get(self.prefix + "FirstName").value = person.FirstName;
	    mc.get(self.prefix + "PhoneNumber").value = person.PhoneNumber;
	    self.button("Return");
	    view.showDetails(self.prefix + "Details");
	},
	fillList : function() {
	    if (mc.get(self.prefix + "Table").rows.length > 1) {
		mc.clearTable(self.prefix + "Table");
	    }
	    Person.sort(Person.SortBy);
	    view.pagination(Person, personcontroller);
	    if ((Person.Position + view.pageSize) > Person.Array.length) {
		var end = Person.Array.length;
	    } else {
		var end = Person.Position + view.pageSize;
	    }
	    for (var i = Person.Position, x = 1; i < end; i++, x++) {
		var row = mc.get(self.prefix + "Table").insertRow(x);
		var person = Person.Array[i];
		row.id = "row" + person.Id;
		row.innerHTML = self.makeRow(person);
	    }
	    view.paginationStatus((Person.Position + 1), Person.Array.length,
		    "personTable");
	    view.searchBar(Person, personcontroller, "Name");
	    view.setTableRowColour(self.prefix + "Table");
	    view.displayList(self.prefix + "List");
	    self.button("Add");
	},
	insertCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		var person = Person.wrap(responseObject.obj);
		person.insert();
		var row = mc.get(self.prefix + "Table").insertRow();
		row.id = "row" + person.Id;
		row.innerHTML = self.makeRow(person);
		view.setTableRowColour(self.prefix + "Table");
		self.button("Add");
		mc.removeClassAttribute("refreshButton", "noDisplay");
		view.displayList(self.prefix + "List");
	    } else {
		view.messagesDisplay(responseObject.ee);
	    }
	},
	makeRow : function(person) {
	    person = Person.wrap(person);
	    return "<td class='rightAlign' id='personIdCol"
		    + person.Id
		    + "'>"
		    + person.Id
		    + "</td><td class='leftAlign' id='personNameCol"
		    + person.Id
		    + "'>"
		    + view.makeLink("personcontroller.edit", person.Id,
			    person.FirstName)
		    + "</td><td class='rightAlign' id='personPhoneCol"
		    + person.Id + "'>" + person.PhoneNumber + "</td>";
	},
	remove : function() {
	    var requestObject = {
		fn : "deletePerson",
		Id : mc.valueOf(self.prefix + "Id"),
	    };
	    mc.ajax(requestObject, self.removeCallback)
	},
	removeCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		var person = Person.wrap(requestObject);
		person.remove();
		mc.removeRow(self.prefix + "Table", person.Id)
		person = Person.wrap(Person.Array[(Person.Position
			+ view.pageSize - 1)]);
		var row = mc.get(self.prefix + "Table").insertRow();
		row.id = "row" + person.Id;
		row.innerHTML = self.makeRow(person);
		self.button("Add");
		view.setTableRowColour(self.prefix + "Table");
		view.displayList(self.prefix + "List");
	    } else {
		mc.get("messages").innerHTML = "Unable to delete this person. Please try again.";
	    }
	},
	reset : function() {
	    mc.get("keyword").value = "";
	    model.peoplePosition = 0;
	    var requestObject = {
		fn : "selectPeople",
	    };
	    mc.ajax(requestObject, self.resetCallback);
	},
	resetCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		Person.Array = responseObject.people;
		Person.SortBy = Person.SortById;
		self.fillList();
	    }
	},
	update : function() {
	    if (mc.valueOf(self.prefix + "Id") == 0) {
		var serverFunction = "insertPerson";
		var callBack = self.insertCallback;
	    } else {
		var serverFunction = "updatePerson";
		var callBack = self.updateCallback
	    }
	    var requestObject = {
		fn : serverFunction,
		Id : mc.valueOf(self.prefix + "Id"),
		FirstName : mc.valueOf(self.prefix + "FirstName"),
		PhoneNumber : mc.valueOf(self.prefix + "PhoneNumber")
	    };
	    mc.ajax(requestObject, callBack);
	},
	updateCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		var person = Person.wrap(responseObject.obj);
		person.update();
		mc.get("row" + person.Id).innerHTML = self.makeRow(person);
		view.setTableRowColour(self.prefix + "Table");
		self.button("Add");
		mc.removeClassAttribute("refreshButton", "noDisplay");
		view.displayList(self.prefix + "List");
	    } else {
		view.messagesDisplay(responseObject.ee);
	    }
	},
	zz_person : 1
    };
    return self;
})();