//
"use strict";
//

var departmentcontroller = (function() {
    var self = {
	prefix : "Department",
	button : function(type) {
	    var button = mc.get("navButton");
	    if (type === "Add") {
		button.innerHTML = "Add " + self.prefix;
		button.onclick = function() {
		    self.edit(0);
		};
	    } else {
		button.innerHTML = "Return"
		button.onclick = function() {
		    self.button("Add");
		    view.displayList(self.prefix + "List");
		};
	    }
	},
	edit : function(departmentId) {
	    var department = Department.select(departmentId);
	    if (department.Id != 0) {
		mc.removeClassAttribute(self.prefix + "DeleteButton",
			"noDisplay");
	    } else {
		mc.displayNone(self.prefix + "DeleteButton");
	    }
	    mc.get(self.prefix + "Id").value = department.Id;
	    mc.get(self.prefix + "Name").value = department.Name;
	    view.personDropDown(self.prefix + "Manager", department.Manager);
	    window.setTimeout(view.personDropDown(self.prefix + "Manager",
		    department.Manager), 20);
	    self.button("Return");
	    view.showDetails(self.prefix + "Details");
	},
	fillList : function() {
	    if (mc.get(self.prefix + "Table").rows.length > 1) {
		mc.clearTable(self.prefix + "Table");
	    }
	    view.pagination(Department, departmentcontroller);
	    Department.sort(Department.SortBy);
	    if ((Department.Position + view.pageSize) > Department.Array.Length) {
		var end = Department.Array.Length;
	    } else {
		var end = Department.Position + view.pageSize;
	    }
	    for (var i = Department.Position, x = 1; i < end; i++, x++) {
		var row = mc.get(self.prefix + "Table").insertRow(x);
		var department = Department.Array[i];
		row.id = "row" + department.Id;
		row.innerHTML = self.makeRow(department);
	    }
	    view.paginationStatus((Department.Position + 1),
		    Department.Array.length, self.prefix + "Table");
	    view.searchBar(Department, departmentcontroller, "Name");
	    view.setTableRowColour(self.prefix + "Table");
	    self.button("Add");
	    view.displayList(self.prefix + "List");
	},
	insertCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		var department = Department.wrap(responseObject.obj);
		department.insert();
		var row = mc.get(self.prefix + "Table").insertRow();
		row.id = "row" + department.Id;
		row.innerHTML = self.makeRow(department);
		view.setTableRowColour(self.prefix + "Table");
		self.button("Add");
		view.displayList(self.prefix + "List");

	    } else {
		view.messagesDisplay(responseObject.ee);
	    }
	},
	makeRow : function(department) {
	    department = Department.wrap(department);
	    return "<td class='rightAlign' id='departmentIdCol"
		    + department.Id
		    + "'>"
		    + department.Id
		    + "</td><td class='leftAlign' id='departmentNameCol"
		    + department.Id
		    + "'>"
		    + view.makeLink("departmentcontroller.edit", department.Id,
			    department.Name)
		    + "</td><td id='departmentManagerCol" + department.Id
		    + "'>" + department.getManager() + "</td>";
	},
	remove : function() {
	    var requestObject = {
		fn : "deleteDepartment",
		Id : mc.valueOf(self.prefix + "Id"),
	    };
	    mc.ajax(requestObject, self.removeCallback);
	},
	removeCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		mc.removeRow(self.prefix + "Table", requestObject.Id)
		var department = Deparment.wrap(requestObject);
		department.remove();
		mc.removeRow(self.prefix + "Table", department.Id);
		department = Department.wrap(Department.Array[(Person.Position
			+ view.pageSize - 1)]);
		var row = mc.get(self.prefix + "Table").insertRow();
		row.id = "row" + department.Id;
		row.innerHTML = self.makeRow(department);
		view.setTableRowColour(self.prefix + "Table");
		self.button("Add");
		view.displayList(self.prefix + "List");
	    } else {
		mc.get("messages").innerHTML = "Unable to delete this person. Please try again.";
	    }
	},
	reset : function() {
	    mc.get("keyword").value = "";
	    var requestObject = {
		fn : "selectDepartments",
		sort : 1
	    };
	    mc.ajax(requestObject, self.resetCallback);
	},
	resetCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		Department.Array = responseObject.departments;
		Department.SortBy = Department.SortById;
		self.fillList();
	    }
	},
	update : function() {
	    if (mc.valueOf(self.prefix + "Id") == 0) {
		var serverFunction = "insertDepartment";
		var callBack = self.insertCallback;
	    } else {
		var serverFunction = "updateDepartment";
		var callBack = self.updateCallback
	    }
	    var requestObject = {
		fn : serverFunction,
		Id : mc.valueOf(self.prefix + "Id"),
		Name : mc.valueOf(self.prefix + "Name"),
		Manager : mc.valueOf(self.prefix + "Manager")
	    };
	    mc.ajax(requestObject, callBack);
	},
	updateCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		var department = Department.wrap(responseOjbect.obj);
		department.update();
		mc.get("row" + department.Id).innerHTML = self.makeRow(person);
		view.setTableRowColour(self.prefix + "Table");
		self.button("Add");
		view.displayList(self.prefix + "List");
	    } else {
		view.messagesDisplay(responseObject.ee);
	    }
	},
	zz_department : 1
    };
    return self;
})();