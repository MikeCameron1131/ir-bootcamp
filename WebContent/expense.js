//
"use strict";
//
var Expense = function(obj) {
    this.Id = obj == null ? 0 : obj.Id;
    this.Description = obj == null ? 0 : obj.Description;
    this.Amount = obj == null ? 0 : obj.Amount;
    this.Payer = obj == null ? 0 : obj.Payer;
};
Expense.Array = [];
Expense.Hash = {};
Expense.SortById = 1;
Expense.SortByDescription = 2;
Expense.SortByAmount = 3;
Expense.SortByPayer = 4;
Expense.SortBy = Expense.SortById;
Expense.Position = 0;
Expense.filter = function(keyword) {
    	var klc = keyword.toLowerCase();
	Expense.Array = Expense.Array.filter(function(expense){
	    var result = expense.Description.toLowerCase().indexOf(klc);
	    return result > -1;
	});
};
Expense.select = function(id){
    return Expense.Hash[id + ""] || {Id:id,Description:"",Amount:0,Payer:0};
};
Expense.sort = function(SortBy) {
    if(SortBy == 2){
	Expense.Array.sort(function(a,b){
	    return a.Description.localeCompare(b.Description);
	});
    } else if (SortBy == 3){
	Expense.Array.sort(function(a,b){
	    return a.Amount - b.Amount;
	});
    } else if (SortBy == 4) {
	Expense.Array.sort(function(a,b){
	    var nameA = Expense.Hash[""+a.Id].getPayer();
	    var nameB = Expense.Hash["" + b.Id].getPayer();
	    var result = nameA.localeCompare(nameB);
	    if(result == 0){
		return a.Description.localeCompare(b.Description);
	    }
	    return result;
	});
    } else {
	Expense.Array.sort(function(a,b){
	    return a.Id - b.Id;
	});
    }
};
Expense.prototype = {
    getPayer : function() {
	if(this.Payer == 0){
	    return "None";
	}else{
	    var person = Person.Hash[this.Payer];
	    if(person == undefined){
		return "None";
	    }else{
		return person.FirstName;
	    }
	}
    },
    insert : function() {
	Expense.Array.push(this);	
	Expense.Hash["" + this.Id] = this;
    },
    remove : function() {
	var index = Expense.Array.findIndex(expense => expense.Id == this.Id);
	Expense.Array.splice(index, 1);
    },
    update : function() {
	var index = model.expenses.findIndex(expense => expense.Id == this.Id);
	Expense.Array[index] = this;
	Expense.Hash["" + this.Id] = this;
    },
    zz_Expense : 1
};
Expense.wrap = function(jsonObj){
    if(jsonObj.insert) {
	return jsonObj;
    }
    return new Expense(jsonObj);
};