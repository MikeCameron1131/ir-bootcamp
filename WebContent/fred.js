//
"use strict";
//
var model = {
    countries : [],
    departments : [],
    expenses : [],
    detailIds : [ "CountryDetails", "DepartmentDetails", "ExpenseDetails",
	    "personDetails" ],
    listIds : [ "CountryList", "DepartmentList", "ExpenseList", "personList" ],
    zz_model : 1
};
//
var view = {
    pageSize : 10,
    aaLoad : function() {
	personcontroller.fillList();
    },
    buildMap : function(constructor) {
	var a = constructor.Array;
	for (var i = 0, z = a.length; i < z; i++) {
	    var obj = a[i] = constructor.wrap(a[i]);
	    constructor.Hash["" + obj.Id] = obj;
	}
    },
    displayList : function(list) {
	mc.removeClassAttribute("searchForm", "noDisplay");
	mc.hideAll(model.detailIds);
	mc.hideAll(model.listIds);
	mc.removeClassAttribute("pagination", "noDisplay");
	mc.removeClassAttribute("paginationStatus", "noDisplay");
	mc.removeClassAttribute(list, "noDisplay");
	view.messagesClear();
    },
    showDetails : function(details) {
	mc.displayNone("searchForm");
	mc.hideAll(model.listIds);
	mc.displayNone("pagination");
	mc.displayNone("paginationStatus");
	mc.removeClassAttribute(details, "noDisplay");
    },
    makeLink : function(onClickFunction, parameter, displayValue) {
	var htm = "<a class='btnLink' onclick='" + onClickFunction + "("
		+ parameter + ")'>" + displayValue + "</a>";
	return htm;
    },
    messagesClear : function() {
	mc.get("messages").innerHTML = "";
    },
    messagesDisplay : function(messages) {
	var count = mc.countByClass("alert");
	for (var i = 0; i < count.length; i++) {
	    mc.removeClassAttribute(count[i].id, "alert");
	}
	var message = "";
	for (var x = 0; x < messages.length; x++) {
	    var messageObject = messages[x]
	    mc.setAlert(messageObject.fieldId);
	    message += messageObject.message + "<br>";
	}
	mc.get("messages").innerHTML = message;
    },
    pagination : function(dataObj, controller) {
	mc.get('paginationFirst').onclick = function() {
	    dataObj.Position = 0;
	    controller.fillList();
	}
	mc.get('paginationLast').onclick = function() {
	    dataObj.Position = dataObj.Array.length - view.pageSize;
	    controller.fillList();
	}
	mc.get('paginationBack').onclick = function() {
	    if (!(dataObj.Position - view.pageSize < 0)) {
		dataObj.Position = dataObj.Position - view.pageSize;
		controller.fillList();
	    }
	}
	mc.get('paginationNext').onclick = function() {
	    if (!(dataObj.Position + view.pageSize >= dataObj.Array.length)) {
		dataObj.Position = dataObj.Position + view.pageSize;
		controller.fillList();
	    }
	}
    },
    paginationStatus : function(start, total, table) {
	var len = mc.get(table).rows.length - 2;
	var htm = "Showing Records " + start + " to " + (start + len) + " of "
		+ total;
	mc.get("paginationStatus").innerHTML = htm;
    },
    personDropDown : function(dropdown, selected) {
	var element = mc.get(dropdown);
	element.options.length = 0;
	var defaultOption = new Option("Select", 0);
	element.options.add(defaultOption);
	defaultOption.selected = true;
	Person.sort(Person.SortByName);
	Person.Array.forEach(function(result) {
	    var option = new Option(result.FirstName, result.Id);
	    element.options.add(option);
	    if (result.Id == selected) {
		option.selected = true;
	    }
	});
    },
    searchBar : function(DataObj, controller, SearchBy) {
	mc.get("searchName").innerHTML = "Search By " + SearchBy;
	mc.get("keyword").innerHTML = ""
	mc.get("reset").onclick = controller.reset;
	mc.get("submit").onclick = function() {
	    DataObj.filter(mc.valueOf("keyword"));
	    controller.fillList();
	}
    },
    setTableRowColour : function(table) {
	for (var i = 0, row; row = mc.get(table).rows[i]; i++) {
	    row.classList.remove("trGreen");
	    if (i % 2 !== 0) {
		row.classList.add("trGreen");
	    }
	}
    },
    zz_view : 1
};
