//
"use strict";
//
var Person = function(obj) {
    this.Id = obj == null ? 0 : obj.Id;
    this.FirstName = obj == null ? "" : obj.FirstName; 
    this.PhoneNumber = obj == null ? 0 : obj.PhoneNumber;
};
// static properties
Person.Array=[];
Person.Hash={};
Person.SortById=1;
Person.SortByName=2;
Person.SortByPhone=3;
Person.Position = 0;
Person.SortBy = Person.SortById;
Person.filter = function(keyword){
    var klc = keyword.toLowerCase();
    Person.Array = Person.Array.filter(function(people){
	var result = people.FirstName.toLowerCase().indexOf(klc);
	return result > -1;
    });
}; 
Person.select = function(id) {
  return Person.Hash[id + ""] || {Id:id,FirstName:"",Phone:0};
};
Person.sort = function(SortBy) {
    	if(SortBy == Person.SortByName){
	    Person.Array.sort(function(a,b){
		return a.FirstName.localeCompare(b.FirstName);
	    });
	}else if(SortBy == Person.SortByPhone){
	    Person.Array.sort(function(a,b){
		return a.PhoneNumber - b.PhoneNumber;
	    });
	}else{
	    Person.Array.sort(function(a,b){
		return a.Id - b.Id;
	    });
	}
};
Person.prototype = {
	 insert : function() {
	     Person.Array.push(this);
	     Person.Hash["" + this.Id] = this;
	    },
	    remove : function() {
		var index = Person.Array.findIndex(person => person.Id == this.Id);
		Person.Array.splice(index, 1);
	    },
	    update : function() {
		var index = Person.Array.findIndex(person => person.Id == this.Id);
		Person.Array[index] = this;
		Person.Hash[this.Id +""] = this;
	    },
    zz_Person : 1
};
Person.wrap = function(jsonObj) {
    if (jsonObj.insert) {
	return jsonObj;
    }
    return new Person(jsonObj);
};