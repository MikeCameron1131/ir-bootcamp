<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ page import="c.web.NavigationBar" %>
<%NavigationBar nav = new NavigationBar(); %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Whoops</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<%=nav.navBar("error.jsp") %>
<h3>Whoops! It looks like something went wrong.  </h3>
<a href="person.jsp">Click here to return to the index page.</a>
</body>
</html>