"use strict";
//
var view = {
    messages : []
};
//
function aaLoad(id) {
    if (id == 0) {
	console.log(id);
	mc.disable('deleteBtn');
	mc.displayNone('deleteBtn');
	mc.displayNone('deleteColumn');
    }
    var length = view.messages.length;
    if (length == 0) {
	mc.focus('name');
    } else {
	var Error = view.messages[0];
	if (Error.error) {
	    mc.setAlert(Error.fieldId);
	    mc.focus(Error.fieldId);
	}
    }
}
