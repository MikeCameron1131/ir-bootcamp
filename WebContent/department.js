//
"use strict";
//
var Department = function(obj) {
    this.Id = obj == null ? 0 : obj.Id;
    this.Name = obj == null ? 0 : obj.Name;
    this.Manager = obj == null ? 0 : obj.Manager;
};
Department.Array=[];
Department.Hash={};
Department.SortById = 1;
Department.SortByName = 2;
Department.SortByManager = 3;
Department.SortBy = Department.SortById;
Department.Position = 0;
Department.filter = function(keyword){
    var klc = keyword.toLowerCase();
    Department = Department.Array.filter(function(department){
	var result = department.Name.toLowerCase().indexOf(klc);
	return result > -1;
    });
}
Department.select = function(id){
    return Department.Hash[id + ""] || {Id:id,Name:"",Manager:0};
}
Department.sort = function(SortBy){
    if(SortBy == Department.SortByName){
	Department.Array.sort(function(a,b){
	    return a.Name.localeCompare(b.Name);
	});
    }else if(SortBy == Department.SortByManager){
	 Department.Array.sort(function(a,b){
	     var managerA = Department.Hash[""+a.Id].getManager();
	     var managerB = Department.Hasg[""+b.Id].getManager();
	     var result =  managerA.localeCompare(managerB);
	     if(result == 0){
		 return a.Name.localeCompare(b.Name);
	     }
	     return result;
	 });
    }else {
	model.departments.sort(function(a,b){
	    return a.Id - b.Id;
	});
    }
}
Department.prototype = {
    getManager : function() {
	if(this.Manager == 0){
	    return "None";
	}else{
	   var person = Person.Hash[""+this.Manager];
	   if(person == undefined){
	       return "None"
	   }else{
	       return person.FirstName;    
	   }
	}
    },
    insert : function() {
	Department.Array.push(this);
	Department.Hash[""+this.Id] == this;
    },
    remove : function() {
	var index = Department.Array.findIndex(department => department.Id == this.Id);
	Department.Array.splice(index, 1);
    },
    update : function() {
	var index = Department.Array.findIndex(department => department.Id == this.Id);
	Department.Array[index] = this;
	Department.Hash[""+this.Id];
    },
    zz_Department : 1
};
Department.wrap = function(jsonObj){
    if(jsonObj.insert) {
	return jsonObj;
    }
    return new Department(jsonObj)
}