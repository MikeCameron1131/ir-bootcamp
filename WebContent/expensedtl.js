"use strict";
//
var view = {
  messages:[]		
};
//
function aaLoad(id){
	var length = view.messages.length;
	if(length == 0)
	{
		mc.focus("description");
	}
	else
	{
		for(var i = length - 1; i >= 0; i--)
		{
			var Error = view.messages[i];
			if(Error.error)
			{
				mc.setAlert(Error.fieldId);
				mc.focus(Error.fieldId);
			}
		}
	}
	if(id == 0)
	{	
	    mc.disable('deleteBtn');
		mc.displayNone('deleteBtn');
		mc.displayNone('deleteColumn');
	}
} 