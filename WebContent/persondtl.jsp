<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="c.web.PersonDetailsBean" %>
<%PersonDetailsBean bean = new PersonDetailsBean(request, response);%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css">
<title><%=bean.person.FirstName == null ? "Add Person" : bean.person.FirstName%></title>
</head>
<script src='persondtl.js'></script>
<script src='mc.js'></script>
<script>
view.messages = <%= bean.getMessageJson() %>;
</script>
<body onLoad="aaLoad(<%=bean.person.Id%>)">
<%=bean.nav.navBar(bean.url)%>
<%if(bean.showDeleteForm) 
{%>
	<form name='deletePerson' method='post' action='persondtl.jsp?id=<%=bean.person.Id%>'>
	<input type="hidden" name="sort" value="<%=bean.getInt("sort") %>"/>  
	<input type="hidden" name="keyword"	value="<%=bean.getString("keyword") %>"/>
	<table class="noBorder">
		<tr class="noBorder"><td><strong>You are about to delete this Person! Do you want to proceed?</strong></td></tr>
		<tr class="noBorder"><td><input class='deleteBtn' type='submit' name="Delete" value='Yes'/>
		<input type='submit' name="Delete" value='No'/></td></tr>
	</table>
	</form>
<%}
else {%>
<div class="centerAlign"><%=bean.getMessageHtm()%></div>
<form name='personForm' method='post' action='persondtl.jsp?id=<%=bean.person.Id%>'>
	<input type="hidden" name="sort" value="<%=bean.getInt("sort") %>"/>  
	<input type="hidden" name="keyword"	value="<%=bean.getString("keyword") %>"/>
	<table>
		<tr><td><label for="name" id='namelabel'>First Name: </label>
		<input type='text' id='name' name='name' value='<%=bean.person.FirstName == null ? "" : bean.person.FirstName%>'/></td></tr>
		<tr><td><label for='phone' id='phonelabel'>Phone Number: </label>
		<input type='text' id="phone" name='phone' value='<%=bean.person.PhoneNumber%>' /></td></tr>
		<tr id="updateRow"><td id ="updateColumn"><input type='submit' id='updateBtn' name='Update' value='Update'/></td>
		<td id ="deleteColumn"><input class='deleteBtn' type='submit' id='deleteBtn' name='Delete' value='Delete'/></td></tr>
	</table>
</form>
<%} %>
<a href = "person.jsp?sort=<%=bean.getInt("sort")%>&keyword=<%=bean.getString("keyword")%>" class="btn addBtn btnCenter">Return to Person List</a>
</body>
</html>