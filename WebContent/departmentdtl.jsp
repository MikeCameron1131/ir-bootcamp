<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="c.web.DepartmentDetailsBean" %>
<%DepartmentDetailsBean bean = new DepartmentDetailsBean(request, response);%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css">
<title><%=bean.department.Name.equals(null) ? "Add Department" : bean.department.Name%></title>
</head>
<script src='departmentdtl.js'></script>
<script src='mc.js'></script>
<script>
view.messages = <%= bean.getMessageJson() %>;
</script>
<body onLoad="aaLoad(<%=bean.department.Id%>)">
<%=bean.nav.navBar(bean.url)%>
<%if(bean.showDeleteForm) 
{%>
	<form name='deletePerson' method='post' action='departmentdtl.jsp?id=<%=bean.department.Id%>'>
	<input type="hidden" name="sort" value="<%=bean.getInt("sort") %>"/>  
	<input type="hidden" name="keyword"	value="<%=bean.getString("keyword") %>"/>
	<table class="noBorder">
		<tr class="noBorder"><td><strong>You are about to delete this Department! Do you want to proceed?</strong></td></tr>
		<tr class="noBorder"><td><input class='deleteBtn' type='submit' name="Delete" value='Yes'/>
		<input type='submit' name="Delete" value='No'/></td></tr>
	</table>
	</form>
<%}
else {%>
<div class="centerAlign"><%=bean.getMessageHtm()%></div>
<form name='personForm' method='post' action='departmentdtl.jsp?id=<%=bean.department.Id%>'>
	<input type="hidden" name="sort" value="<%=bean.getInt("sort") %>"/>  
	<input type="hidden" name="keyword"	value="<%=bean.getString("keyword") %>"/>
	<table>
		<tr><td><label for="namelabel" id='namelabel'>Department: </label>
		<input type='text' id='name' name='name' value='<%=bean.department.Name == null ? "" : bean.department.Name%>'/></td></tr>
		<tr><td><label for='manager' id='managerlabel'>Manager: </label>
		<select id='manager' name='manager'><%=bean.dropdown%></select></td></tr>
		<tr id="updateRow"><td id ="updateColumn"><input type='submit' id='updateBtn' name='Update' value='Update'/></td>
		<td id ="deleteColumn"><input class='deleteBtn' type='submit' id='deleteBtn' name='Delete' value='Delete'/></td></tr>
	</table>
</form>
<%} %>
<a href = "department.jsp?sort=<%=bean.getInt("sort")%>&keyword=<%=bean.getString("keyword")%>" class="btn addBtn btnCenter">Return to Department List</a>
</body>
</html>