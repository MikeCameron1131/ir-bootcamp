<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="c.web.ExpenseDetailsBean" %>
<%ExpenseDetailsBean bean = new ExpenseDetailsBean(request, response);%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css">
<title><%=bean.expense.Description == null ? "Add Expense" : bean.expense.Description%></title>
</head>
<script src='expensedtl.js'></script>
<script src='mc.js'></script>
<script>
view.messages = <%= bean.getMessageJson() %>;
</script>
<body onLoad="aaLoad(<%=bean.expense.Id%>)">
<%=bean.nav.navBar(bean.url)%>
<%if(bean.showDeleteForm) 
{%>
	<form name='deletePerson' method='post' action='expensedtl.jsp?id=<%=bean.expense.Id%>'>
	<input type="hidden" name="sort" value="<%=bean.getInt("sort") %>"/>  
	<input type="hidden" name="keyword"	value="<%=bean.getString("keyword") %>"/>
	<table class="noBorder">
		<tr class="noBorder"><td><strong>You are about to delete this Expense! Do you want to proceed?</strong></td></tr>
		<tr class="noBorder"><td><input style='background-color:#d00011;color:#ffffff;' type='submit' name="Delete" value='Yes'/>
		<input type='submit' name="Delete" value='No'/></td></tr>
	</table>
	</form>
<%}
else {%>
<div class="centerAlign"><%=bean.getMessageHtm()%></div>
<form name='personForm' method='post' action='expensedtl.jsp?id=<%=bean.expense.Id%>'>
	<input type="hidden" name="sort" value="<%=bean.getInt("sort") %>"/>  
	<input type="hidden" name="keyword"	value="<%=bean.getString("keyword") %>"/>
	<table>
		<tr><td><label for="description" id='descriptionlabel'>Description: </label>
		<textarea id='description' name='description' rows='4' cols='30'><%=bean.expense.Description == null ? "" : bean.expense.Description%></textarea></td></tr>
		<tr><td><label for='amount' id='amountlabel'>Amount: </label>
		<input type='number' id="amount" name='amount' step='0.01' min='0' value='<%=bean.expense.Amount%>' /></td>
		<tr><td><label for=payer' id='payerlabel'>Payer: </label>
		<select id='payer' name='payer'><%=bean.dropdown%></select></td></tr>
		<tr id="updateRow"><td id ="updateColumn"><input type='submit' id='updateBtn' name='Update' value='Update'/></td>
		<td id ="deleteColumn"><input class='deleteBtn' type='submit' id='deleteBtn' name='Delete' value='Delete'/></td></tr>
	</table>
</form>
<%} %>
<a href = "expense.jsp?sort=<%=bean.getInt("sort")%>&keyword=<%=bean.getString("keyword")%>" class="btn addBtn btnCenter">Return to Expense List</a>
</body>
</html>

