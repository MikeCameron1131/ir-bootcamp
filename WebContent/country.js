//
"use strict";
//
var Country = function(obj) {
    this.Id = obj == null ? 0 : obj.Id;
    this.Name = obj == null ? "" : obj.Name;
};
Country.Array = [];
Country.Hash = {};
Country.SortById = 1;
Country.SortByName = 2;
Country.Position = 0;
Country.SortBy = Country.SortById;
//
Country.filter = function(keyword) {
    var klc = keyword.toLowerCase();
   Country.Array = Country.Array.filter(function(country) {
	var result = country.Name.toLowerCase().indexOf(klc);
	return result > -1;
    });
};
Country.select = function(id) {
    return Country.Hash[id + ""] || {
	Id : id,
	Name : ""
    };
};
Country.sort = function(SortBy) {
    if (SortBy == Country.SortById) {
	Country.Array.sort(function(a, b) {
	    return a.Id - b.Id;
	});
    } else {
	Country.Array.sort(function(a, b) {
	    return a.Name.localeCompare(b.Name);
	});
    }
};
Country.prototype = {
    insert : function() {
	Country.Array.push(this);
	Country.Hash["" + this.Id] == this;
    },
    remove : function() {
	var index = Country.Array.findIndex(country => country.Id == this.Id)
	Country.Array.splice(index, 1);
    },
    update : function() {
	var index = Country.Array.findIndex(country => country.Id == this.Id)
	Country.Array[index] = this;
	Person.Hash[this.Id + ""] = this;
    },
    zz_country : 1
};
Country.wrap = function(jsonObj) {
    if (jsonObj.insert) {
	return jsonObj;
    }
    return new Country(jsonObj);
};