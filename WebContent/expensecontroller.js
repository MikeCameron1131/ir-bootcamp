//
"use strict";
//

var expensecontroller = (function() {
    var self = {
	prefix : "Expense",
	button : function(type) {
	    var button = mc.get("navButton");
	    if (type === "Add") {
		button.innerHTML = "Add " + self.prefix
		button.onclick = function() {
		    self.edit(0);
		};
	    } else {
		button.innerHTML = "Return"
		button.onclick = function() {
		    self.button("Add");
		    view.displayList(self.prefix + "List");
		};
	    }
	},
	edit : function(expenseId) {
	    var expense = Expense.select(expenseId);
	    if (expenseId != 0) {
		mc.removeClassAttribute(self.prefix + "DeleteButton",
			"noDisplay");
	    } else {
		mc.displayNone(self.prefix + "DeleteButton");
	    }
	    mc.get(self.prefix + "Id").value = expense.Id;
	    mc.get(self.prefix + "Description").value = expense.Description;
	    mc.get(self.prefix + "Amount").value = expense.Amount;
	    view.personDropDown(self.prefix + "Payer", expense.Payer);
	    expensecontroller.button("Return");
	    view.showDetails(self.prefix + "Details");
	},
	fillList : function() {
	    if (mc.get(self.prefix + "Table").rows.length > 1) {
		mc.clearTable(self.prefix + "Table");
	    }
	    Expense.sort(Expense.SortBy);
	    view.pagination(Expense, expensecontroller);
	    if ((Expense.Position + view.pageSize) > Expense.Array.length) {
		var end = Expense.Array.length;
	    } else {
		var end = Expense.Position + view.pageSize;
	    }
	    for (var i = Expense.Position, x = 1; i < end; i++, x++) {
		var expense = Expense.Array[i];
		var row = mc.get(self.prefix + "Table").insertRow(x);
		row.id = "row" + expense.Id;
		row.innerHTML = self.makeRow(expense);
	    }
	    view.paginationStatus((Expense.Position + 1), Expense.Array.length,
		    self.prefix + "Table");
	    view.searchBar(Expense, expensecontroller, "Description");
	    view.setTableRowColour(self.prefix + "Table");
	    view.displayList(self.prefix + "List");
	    self.button("Add");
	},
	insertCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		var expense = Expense.wrap(responseObject.obj);
		var row = mc.get(self.prefix + "Table").insertRow();
		row.id = "row" + expense.Id;
		row.innerHTML = self.makeRow(expense);
		view.setTableRowColour(self.prefix + "Table");
		self.button("Add");
		mc.removeClassAttribute("refreshButton", "noDisplay");
		view.displayList(self.prefix + "List");
	    } else {
		view.messagesDisplay(responseObject.ee);
	    }
	},
	makeRow : function(expense) {
	    expense = Expense.wrap(expense);
	    return "<td class='rightAlign' id='expenseIdCol"
		    + expense.Id
		    + "'>"
		    + expense.Id
		    + "</td><td class='leftAlign' id='expenseDescriptionCol"
		    + expense.Id
		    + "'>"
		    + view.makeLink("expensecontroller.edit", expense.Id,
			    expense.Description)
		    + "</td><td class='rightAlign' id='expenseAmountCol"
		    + expense.Id + "'>" + expense.Amount.toFixed(2) + "</td>"
		    + "</td><td class='leftAlign' id='expensePayerCol"
		    + expense.Id + "'>" + expense.getPayer() + "</td>";
	},
	remove : function() {
	    var requestObject = {
		fn : "deleteExpense",
		Id : mc.valueOf(self.prefix + "Id"),
	    };
	    mc.ajax(requestObject, self.removeCallback)
	},
	removeCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		var expense = Expense.wrap(requestObject);
		expense.remove();
		mc.removeRow(self.prefix + "Table", expense.Id)
		expense = Expense.wrap(Expense.Array[(Expense.Position
			+ view.pageSize - 1)]);
		var row = mc.get(self.prefix + "Table").insertRow();
		row.id = "row" + expense.Id;
		row.innerHTML = seld.makeRow(expense);
		self.button("Add");
		view.setTableRowColour(self.prefix + "Table");
		view.displayList(self.prefix + "List");
	    } else {
		mc.get("messages").innerHTML = "Unable to delete this expense. Please try again.";
	    }
	},
	reset : function() {
	    mc.get("keyword").value = "";
	    var requestObject = {
		fn : "selectExpenses",
		sort : 1
	    };
	    mc.ajax(requestObject, self.resetCallback);
	},
	resetCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		model.expenses = responseObject.expenses;
		Expense.SortBy = Expense.SortById;
		self.fillList();
	    }
	},
	update : function() {
	    if (mc.valueOf(self.prefix + "Id") == 0) {
		var serverFunction = "insertExpense";
		var callBack = self.insertCallback;
	    } else {
		var serverFunction = "updateExpense";
		var callBack = self.updateCallback
	    }
	    var requestObject = {
		fn : serverFunction,
		Id : mc.valueOf(self.prefix + "Id"),
		Description : mc.valueOf(self.prefix + "Description"),
		Amount : mc.valueOf(self.prefix + "Amount"),
		Payer : mc.valueOf(self.prefix + "Payer")
	    };
	    mc.ajax(requestObject, callBack);
	},
	updateCallback : function(requestObject, responseObject) {
	    if (responseObject.Ok == 1) {
		var expense = Expense.wrap(responseObject.obj);
		expense.update();
		mc.get("row" + expense.Id).innerHTML = self.makeRow(expense);
		view.setTableRowColour(self.prefix + "Table");
		self.button("Add");
		view.displayList(self.prefix + "List");
	    } else {
		view.messagesDisplay(responseObject.ee);
	    }
	},
	zz_expense : 1
    };
    return self;
})();