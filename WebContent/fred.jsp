<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="c.web.FredBean" %>
<%FredBean bean = new FredBean(request, response);%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css">
<title>Fred</title>
</head>
<script src='fred.js'></script>
<script src='mc.js'></script>
<script src='department.js'></script>
<script src='departmentcontroller.js'></script>
<script src='country.js'></script>
<script src='countrycontroller.js'></script>
<script src='expense.js'></script>
<script src='expensecontroller.js'></script>
<script src='person.js'></script>
<script src='personcontroller.js'></script>
<script>
Person.Array = <%= bean.people %>;
window.setTimeout(view.buildMap(Person),20);
Country.Array = <%= bean.countries %>;
window.setTimeout(view.buildMap(Country),20);
Expense.Array = <%= bean.expenses %>;
window.setTimeout(view.buildMap(Expense),20);
Department.Array = <%= bean.departments %>;
window.setTimeout(view.buildMap(Department),20);
</script>
<body onload='view.aaLoad()'>
<%=bean.nav.navBar(bean.url) %>
<div  style='display:inline-block'>
	<a class='btnLink' onclick='countrycontroller.fillList()'>Countries</a>
	<a class='btnLink' onclick='departmentcontroller.fillList()'>Departments</a>
	<a class='btnLink' onclick='expensecontroller.fillList()'>Expenses</a>
	<a class='btnLink' onclick='personcontroller.fillList()'>People</a>
</div>
<br>

<div id="paginationStatus" class='centerAlign'></div>
<div id ="searchForm">
<table>
	<tr><td id="searchName">Search by Person </td>
	<td><input type='text' name='keyword' id='keyword'/></td>
	<td><button type='button' name='submit' id='submit'>Search</button><td></td>
	<td><button type='button' name='reset' id='reset'>Reset</button></td></tr>
</table>
</div>
<div id="messages" class="centerAlign"></div>
<div id='CountryList' class='noDisplay'>
<table id='CountryTable'>
	<tr>
		<th class='rightAlign btnLink' onclick='Country.SortBy = Country.SortById; countrycontroller.fillList();mc.displayNone("refreshButton");'>Id</th>
		<th class='leftAlign btnLink' onclick='Country.SortBy = Country.SortByName; countrycontroller.fillList();mc.displayNone("refreshButton");'>Name</th>
	</tr>
</table>
</div>
<div id='CountryDetails' class='noDisplay'>
	<form id='CountryForm'>
	<table>
		<input type='hidden' name='Countryid' id='CountryId' />
		<tr>
			<td><label for='CountryName' id='namelabel'>Name: </label></td>
			<td><input type='text' id='CountryName'/></td>
		</tr>
		<tr>
			<td><button type='button' id='CountryUpdateButton' onclick='countrycontroller.update()'>Update</button></td>
			<td><button type='button' id='CountryDeleteButton' onclick='countrycontroller.remove()'>Delete</button></td>
		</tr>
	</table>
	</form>
</div>
<div id='DepartmentList' class='noDisplay'>
<table id='DepartmentTable'>
	<tr>
		<th class='rightAlign btnLink' onclick='Department.SortBy = Department.SortByID; departmentcontroller.fillList();mc.displayNone("refreshButton");'>Id</th>
		<th class='leftAlign btnLink' onclick='Department.SortBy = Department.SortByName; departmentcontroller.fillList();mc.displayNone("refreshButton");'>Name</th>
		<th class='leftAlign btnLink' onclick='Department.SortBy = Department.SortByManager; departmentcontroller.fillList();mc.displayNone("refreshButton");'>Manager</th>
	</tr>
</table>
</div>
<div id='DepartmentDetails' class='noDisplay'>
	<form id='DepartmentForm'>
	<table>
		<input type='hidden' name='id' id='DepartmentId' />
		<tr>
			<td><label for='DepartmentName' id='namelabel'>Name: </label></td>
			<td><input type='text' id='DepartmentName'/></td></tr>
		</tr>
		<tr>
			<td><label for='DepartmentManager' id='managerlabel'>Manager: </label></td>
			<td><select id='DepartmentManager'></select></td>
		</tr>
		<tr>
			<td><button type='button' id='DepartmentUpdateButton' onclick='departmentcontroller.update()'>Update</button></td>
			<td><button type='button' id='DepartmentDeleteButton' onclick='departmentcontroller.remove()'>Delete</button></td>
		</tr>
	</table>
	</form>
</div>
<div id='ExpenseList' class='noDisplay'>
<table id='ExpenseTable'>
	<tr>
		<th class='rightAlign btnLink' onclick='Expense.SortBy = Expense.SortById; expensecontroller.fillList();mc.displayNone("refreshButton");'>Id</th>
		<th class='leftAlign btnLink' onclick='Expense.SortBy = Expense.SortByDescription; expensecontroller.fillList();mc.displayNone("refreshButton");'>Description</th>
		<th class='rightAlign btnLink' onclick='Expense.SortBy = Expense.SortByAmount; expensecontroller.fillList();mc.displayNone("refreshButton");'>Amount</th>
		<th class='leftAlign btnLink' onclick='Expense.SortBy = Expense.SortByPayer; expensecontroller.fillList();mc.displayNone("refreshButton");'>Payer</th>
	</tr>
</table>
</div>
<div id='ExpenseDetails' class='noDisplay'>
	<form id='ExpenseForm'>
	<table>
		<input type='hidden' name='id' id='ExpenseId' />
		<tr>
			<td><label for='ExpenseDescription' id='descriptionlabel'>Description: </label></td>
			<td><textarea id='ExpenseDescription'  rows='4' cols='30'></textarea></td></tr>
		</tr>
		<tr>
			<td><label for='ExpenseAmount' id='amountlabel'>Amount: </label></td>
			<td><input type='number'  step='0.01' min='0'' id='ExpenseAmount' value=''/></td>
		</tr>
		<tr>
			<td><label for='ExpensePayer' id='payerlabel'>Payer: </label></td>
			<td><select id='ExpensePayer'></select></td>
		</tr>
		<tr>
			<td><button type='button' id='ExpenseUpdateButton' onclick='expensecontroller.update()'>Update</button></td>
			<td><button type='button' id='ExpenseDeleteButton' onclick='expensecontroller.remove()'>Delete</button></td>
		</tr>
	</table>
	</form>
</div>
<div id='personList'>
<table id='personTable'>
	<tr>
		<th class='rightAlign btnLink' onclick='Person.SortBy = Person.SortById; personcontroller.fillList(); mc.displayNone("refreshButton");'>Id</th>
		<th class='leftAlign btnLink' onclick='Person.SortBy = Person.SortByName; personcontroller.fillList(); mc.displayNone("refreshButton");'>Name</th>
		<th class='rightAlign btnLink' onclick='Person.SortBy = Person.SortByPhone; personcontroller.fillList(); mc.displayNone("refreshButton");'>Phone</th>
	</tr>
</table>
</div>
<div id='personDetails' class='noDisplay'>
	<form id='personForm'>
	<table>
		<input type='hidden' name='id' id='personId'/>
		<tr>
			<td><label for='personName' id='namelabel'>Name: </label>
			<td><input type='text' id='personFirstName'/></td>
		</tr>
		<tr>
			<td><label for='personPhone' id='phonelabel'>Phone: </label>
			<td><input type='number' id='personPhoneNumber'/></td>
		</tr>
		<tr>
			<td><button type='button' id='personUpdateButton' onclick='personcontroller.update()'>Update</button></td>
			<td><button type='button' id='personDeleteButton' onclick='personcontroller.remove()'>Delete</button></td>
		</tr>
		</tr>
	</table>
	</form>
</div>
<div id="buttons">
	<button type='button' class='btn addBtn btnCenter' id='navButton'></button>
	<button type='button' class='btn refreshBtn btnCenter noDisplay' id='refreshButton' onclick='personcontroller.fillList();'>Refresh Table</button>
</div>
<div id="pagination" class="pagination">
<table>
	<tr>
		<td><a id='paginationFirst' class='btnLink'>First</a></td>
		<td><a id="paginationBack" class='btnLink'>Back</a></td>
		<td><a id="paginationNext" class='btnLink'>Next</a></td>
		<td><a id="paginationLast" class='btnLink'>Last</a></td>
	</tr>
</table>
</div>
<br>
</html>